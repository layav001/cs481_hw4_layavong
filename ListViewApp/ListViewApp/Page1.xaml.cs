﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ListViewApp
{
    public class Info
    {
        public string Name1 { get; set;  }
        public string Pic1 { get; set;  }
    }

	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page1 : ContentPage
	{
        public ObservableCollection<Info> NewList { get; set; }
		public Page1 ()
		{
			InitializeComponent ();
            GetInfo();
		}
        private void GetInfo()
        {
            NewList = new ObservableCollection<Info>()
            {
                new Info()
                {
                    Name1 = "Cancun",
                    Pic1 = "cancun.jpg"
                },

                 new Info()
                {
                    Name1 = "Hawaii",
                    Pic1 = "hawaii.jpg"
                },
                 new Info()
                {
                    Name1 = "Maldives",
                    Pic1 = "maldives.jpg"
                },
                 new Info()
                {
                    Name1 = "Costa Rica",
                    Pic1 = "costarica"
                },
                 new Info()
                {
                    Name1 = "Jamaica",
                    Pic1 = "jamaica"
                },


            };
            lol.ItemsSource = NewList;
        }

      

        private void Lol_Refreshing(object sender, EventArgs e)
        {
            GetInfo();
            lol.IsRefreshing = false;
        }

        private void MenuItem_Clicked(object sender, EventArgs e)
        {
            var menuItem = sender as MenuItem;
            Info item = (Info)menuItem.CommandParameter; 

            if(item.Name1 == "Cancun")
            {
                Navigation.PushAsync(new Cancun());
            }
            else if(item.Name1 == "Hawaii")
            {
                Navigation.PushAsync(new Hawaii());
            }
            else if (item.Name1 == "Maldives")
            {
                Navigation.PushAsync(new Maldives());
            }
            else if (item.Name1 == "Costa Rica")
            {
                Navigation.PushAsync(new CostaRica());
            }
            else if (item.Name1 == "Jamaica")
            {
                Navigation.PushAsync(new Jamaica());
            }
        }

        private void MenuItem_Clicked_1(object sender, EventArgs e)
        {
            var menuItem = sender as MenuItem;
            var delete = (Info)menuItem.CommandParameter;
            NewList.Remove(delete);
          /*  if(menuItem != null)
            {
                var name = menuItem.BindingContext as string; 
            }
            */
        }
    }
}